package translators

import (
	"testing"

	"github.com/go-test/deep"
)

var (
	urls           map[string]GohAddress
	GohAddressKeys []string
)

func init() {
	urls = make(map[string]GohAddress)

	//TODO: Add more tests for several of these
	//https://gitlab.com/theuberlab/emmet/bashcgi101
	//gitlab.com/theuberlab/emmet/learn
	//git@gitlab.com:theuberlab/lug.git
	//https://gitlab.com/theuberlab/lug.git
	//https://github.com/kubernetes/kubernetes.git
	//github.com/kubernetes/website

	urls["git@github.com:prometheus/prometheus.git"] = GohAddress{
		Supplied:  "git@github.com:prometheus/prometheus.git",
		SupScheme: URL_SCHEME_SSH,
		SupDomain: "github.com",
		SupPath:   "/prometheus/prometheus.git",
		LocalPath: "github.com/prometheus/prometheus",
		CloneAddr: "git@github.com:prometheus/prometheus.git",
	}

	urls["github.com/prometheus/alertmanager"] = GohAddress{
		Supplied:  "github.com/prometheus/alertmanager",
		SupScheme: URL_SCHEME_NONE,
		SupDomain: "github.com",
		SupPath:   "/prometheus/alertmanager",
		LocalPath: "github.com/prometheus/alertmanager",
		CloneAddr: "https://github.com/prometheus/alertmanager.git",
	}

	urls["https://github.com/prometheus/client_golang"] = GohAddress{
		Supplied:  "https://github.com/prometheus/client_golang",
		SupScheme: URL_SCHEME_HTTPS,
		SupDomain: "github.com",
		SupPath:   "/prometheus/client_golang",
		LocalPath: "github.com/prometheus/client_golang",
		CloneAddr: "https://github.com/prometheus/client_golang.git",
	}

	urls["https://gitlab.com/theuberlab/boneyard/visiblekitteh"] = GohAddress{
		Supplied:  "https://gitlab.com/theuberlab/boneyard/visiblekitteh",
		SupScheme: URL_SCHEME_HTTPS,
		SupDomain: "gitlab.com",
		SupPath:   "/theuberlab/boneyard/visiblekitteh",
		LocalPath: "gitlab.com/theuberlab/boneyard/visiblekitteh",
		CloneAddr: "https://gitlab.com/theuberlab/boneyard/visiblekitteh.git",
	}

	urls["git@gitlab.com:theuberlab/boneyard/visiblekitteh.git"] = GohAddress{
		Supplied:  "git@gitlab.com:theuberlab/boneyard/visiblekitteh.git",
		SupScheme: URL_SCHEME_SSH,
		SupDomain: "gitlab.com",
		SupPath:   "/theuberlab/boneyard/visiblekitteh.git",
		LocalPath: "gitlab.com/theuberlab/boneyard/visiblekitteh",
		CloneAddr: "git@gitlab.com:theuberlab/boneyard/visiblekitteh.git",
	}

	urls["https://gitlab.com/theuberlab/boneyard/visiblekitteh.git"] = GohAddress{
		Supplied:  "https://gitlab.com/theuberlab/boneyard/visiblekitteh.git",
		SupScheme: URL_SCHEME_HTTPS,
		SupDomain: "gitlab.com",
		SupPath:   "/theuberlab/boneyard/visiblekitteh.git",
		LocalPath: "gitlab.com/theuberlab/boneyard/visiblekitteh",
		CloneAddr: "https://gitlab.com/theuberlab/boneyard/visiblekitteh.git",
	}

	urls["https://gitlab.com/theuberlab/gohget/-/releases"] = GohAddress{
		Supplied:  "https://gitlab.com/theuberlab/gohget/-/releases",
		SupScheme: URL_SCHEME_HTTPS,
		SupDomain: "gitlab.com",
		SupPath:   "/theuberlab/gohget/-/releases",
		LocalPath: "gitlab.com/theuberlab/gohget",
		CloneAddr: "https://gitlab.com/theuberlab/gohget.git",
	}

	urls["gitlab.com/theuberlab/gohget"] = GohAddress{
		Supplied:  "gitlab.com/theuberlab/gohget",
		SupScheme: URL_SCHEME_NONE,
		SupDomain: "gitlab.com",
		SupPath:   "/theuberlab/gohget",
		LocalPath: "gitlab.com/theuberlab/gohget",
		CloneAddr: "https://gitlab.com/theuberlab/gohget.git",
	}

	urls["https://gitlab.com/theuberlab/gohget"] = GohAddress{
		Supplied:  "https://gitlab.com/theuberlab/gohget",
		SupScheme: URL_SCHEME_HTTPS,
		SupDomain: "gitlab.com",
		SupPath:   "/theuberlab/gohget",
		LocalPath: "gitlab.com/theuberlab/gohget",
		CloneAddr: "https://gitlab.com/theuberlab/gohget.git",
	}

	GohAddressKeys = []string{"Supplied", "SupScheme", "SupDomain", "SupPath", "CloneAddr"}
}

func TestParseURL(t *testing.T) {
	for testURL := range urls {
		result := parseURL(testURL)

		if diff := deep.Equal(result, urls[testURL]); diff != nil {
			t.Errorf("Failed to parse %s", testURL)
			t.Error(diff)
		}
	}
}

func TestTranslateAddress(t *testing.T) {
	transCon := NewDefaultController()

	for testURL := range urls {
		result := transCon.TranslateAddress(testURL)

		if diff := deep.Equal(result, urls[testURL]); diff != nil {
			t.Errorf("Failed to parse %s", testURL)
			t.Error(diff)
		}

	}

}
