package translators

import (
	"bufio"
	"io/ioutil"
	"net/http"
	"net/url"
	"regexp"
	"strings"

	"github.com/prometheus/common/log"
	"gitlab.com/theuberlab/lug"
)

// Translator represents all translators
type Translator interface {
	// Takes an address supplied on the command line and translates it to a form valid for git clone
	TranslateAddress(string) string
	// Returns a list of prefixes supported by the translator
	GetPrefixes() []string
}

// GohAddress is an address as gohget sees it.
type GohAddress struct {
	// Exactly what was supplied on the command line.
	Supplied string
	// The Scheme that was supplied
	SupScheme URLScheme
	// The domain portion of the supplied address
	SupDomain string
	// The path to the project supplied
	SupPath string
	// The path to clone the project to
	LocalPath string
	// The resolved URL suitable for git clone
	CloneAddr string
}

// URLScheme is the enum for scheme
type URLScheme int

const (
	// URL_SCHEME_NONE no scheme provided
	URL_SCHEME_NONE URLScheme = iota
	// URL_SCHEME_HTTPS https://
	URL_SCHEME_HTTPS
	// URL_SCHEME_SSH a git ssh type request 'git@github...'
	URL_SCHEME_SSH
	// URL_SCHEME_HTTP really shouldn't be used
	URL_SCHEME_HTTP
	// URL_SCHEME_FILE file://
	URL_SCHEME_FILE
	// URL_SCHEME_ERROR represents an error parsing the scheme
	URL_SCHEME_ERROR = 400 // If we get even close to this many values we're probably doing something wrong
)

// String gets the string representation of a URLScheme
func (p *URLScheme) String() string {
	names := [...]string{
		"URL_SCHEME_NONE",
		"URL_SCHEME_HTTPS",
		"URL_SCHEME_SSH",
		"URL_SCHEME_HTTP",
		"URL_SCHEME_FILE",
		"URL_SCHEME_ERROR",
	}

	if *p < URL_SCHEME_NONE || *p > URL_SCHEME_ERROR {
		return "NONE"
	}

	return names[*p]
}

// SchemeFromString returns a URLScheme from the provided string
func SchemeFromString(pluginString string) URLScheme {
	enums := make(map[string]URLScheme)

	enums["ERROR"] = URL_SCHEME_ERROR
	enums["HTTPS"] = URL_SCHEME_HTTPS
	enums["SSH"] = URL_SCHEME_SSH
	enums["HTTP"] = URL_SCHEME_HTTP
	enums["FILE"] = URL_SCHEME_FILE
	enums["NONE"] = URL_SCHEME_NONE

	_ = lug.Trace("Message", "normalizing string", "String", pluginString)
	pluginString = strings.ToUpper(pluginString)

	_ = lug.Trace("Message", "Normalized", "String", pluginString, "EnumValue", enums[pluginString])

	if pluginString == "" {
		return URL_SCHEME_NONE
	}

	result, ok := enums[pluginString]
	if !ok {
		return URL_SCHEME_ERROR
	}

	return result
}

// TranslatorController routes requests to the correct Translator
type TranslatorController struct {
	Insecure bool
}

// NewDefaultController creates an instance of TranslatorController with default values.
func NewDefaultController() *TranslatorController {
	return &TranslatorController{
		Insecure: false,
	}
}

// parseURL parses the supplied URL returning a GohAddress with the results.
func parseURL(urlToParse string) GohAddress {
	result := GohAddress{Supplied: urlToParse}

	// First determine if there are and leading objects before the domain name. Scheme, credentials, etc.
	var myRegex = regexp.MustCompile(`^(?P<url>((?P<scheme>http(s?))://)?(?P<user>[^@\n]+@)?(?P<host>[^:/]+)(:|/)(?P<path>.*?(?P<gitsuffix>.git)?))$`)

	matches := myRegex.FindStringSubmatch(urlToParse)

	results := make(map[string]string)
	for i, name := range myRegex.SubexpNames() {
		if i != 0 && name != "" {
			results[name] = matches[i]
		}
	}

	result.SupScheme = SchemeFromString(results["scheme"])

	if results["user"] != "" && result.SupScheme == URL_SCHEME_NONE {
		result.SupScheme = URL_SCHEME_SSH
	}

	result.SupDomain = results["host"]
	result.SupPath = "/" + results["path"]

	// Figure out how we will determine the git clone URL to use.
	if result.SupScheme != URL_SCHEME_SSH && strings.Contains(results["host"], "gitlab") {
		// Gitlab URL make the clone URL manually.
		var subPath string
		if strings.Contains(result.SupPath, "/-/") {
			subPath = strings.Split(result.SupPath, "/-/")[0]
		} else {
			subPath = result.SupPath
		}
		subPath = strings.TrimSuffix(subPath, "/")
		subPath = strings.TrimSuffix(subPath, ".git")
		result.LocalPath = result.SupDomain + subPath

		result.CloneAddr = "https://" + result.SupDomain + subPath + ".git"
	} else if result.SupScheme == URL_SCHEME_SSH && results["gitsuffix"] == ".git" {
		// This appears to be a properly formatted ssh style URL. Simply use it explicitely.
		subPath := strings.Split(strings.Split(results["url"], ":")[1], ".gi")[0]
		result.LocalPath = result.SupDomain + "/" + subPath
		result.CloneAddr = results["url"]
	} else {
		// This is neither gitlab nor an ssh url attempt to fetch the go-import meta tag.
		result.CloneAddr = fetchMetaTag(result)
		parsedURL, err := url.Parse(result.CloneAddr)
		if err != nil {
			log.Error(err)
		}
		result.LocalPath = result.SupDomain + strings.Split(parsedURL.Path, ".gi")[0]
	}

	return result
}

// fetchMetaTag fetches the meta tag similar to how go get does.
func fetchMetaTag(address GohAddress) string {
	result := "ERROR"
	var reqURL string
	if address.SupScheme != URL_SCHEME_HTTPS {
		reqURL = "https://" + address.SupDomain + address.SupPath
	} else {
		reqURL = address.Supplied
	}

	reqURL = strings.TrimSuffix(reqURL, "/")

	reqURL = reqURL + "?go-get=1"

	resp, err := http.Get(reqURL)
	if err != nil {
		log.Fatalln(err)
	}

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}

	if len(respBody) == 0 {
		log.Fatalln("Response body is empty")
	}

	stringBody := string(respBody)

	scanner := bufio.NewScanner(strings.NewReader(stringBody))
	for scanner.Scan() {
		if strings.Contains(scanner.Text(), "go-import") {
			result = strings.Split(strings.Split(scanner.Text(), "git ")[1], "\"")[0]
			break
		}
	}

	return result
}

// TranslateAddress sends an address to an appropriate Translator
func (t *TranslatorController) TranslateAddress(address string) GohAddress {
	_ = lug.Trace("Message", "Running TranslateAddress")

	goAddr := parseURL(address)

	return goAddr
}
