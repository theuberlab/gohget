package fetchers

import (
	"gitlab.com/theuberlab/gohget/pkg/translators"
	"log"
	"os/exec"
)

// GitFetch fetches the supplied address with git clone
func GitFetch(address translators.GohAddress, codeRoot string) string {

	destDir := codeRoot + "/" + address.LocalPath

	cmd := exec.Command("git", "clone", address.CloneAddr, destDir)

	err := cmd.Run()
	if err != nil {
		log.Fatal("cmd.Run() failed with " + err.Error())
	}

	return destDir
}
