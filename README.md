[![Pipeline Status](https://gitlab.com/theuberlab/gohget/badges/master/pipeline.svg)](https://gitlab.com/theuberlab/gohget/-/commits/master)
[![coverage](https://gitlab.com/theuberlab/gohget/badges/master/coverage.svg)](https://ggitlab.com/theuberlab/gohget/badges/master/coverage.svg)
[![GoDoc](https://godoc.org/gitlab.com/theuberlab/gohget?status.svg)](https://godoc.org/gitlab.com/theuberlab/gohget)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/theuberlab/gohget)](https://goreportcard.com/report/gitlab.com/theuberlab/gohget)
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
[![pkg.go.dev](https://pkg.go.dev/badge/gitlab.com/theuberlab/gohget)](https://pkg.go.dev/gitlab.com/theuberlab/gohget)


# Goh Get
Easily fetch and organize source code by repository address.

Goh! Get! is a small tool which will clone git source repositories
into a path based on their URL. This codebase for example would be cloned
into `${GOH_CODE_ROOT}/gitlab.com/theuberlab/gohget`

## Usage
GoH! Get! seeks to add flexibility on top of `go get` but function in
a similar fashion.

Simply call gohget and pass the path to a git repository.

Such as:
`gohget gitlab.com/theuberlab/gohget`

And Goh! Get! will clone the repository into `${GOH_CODE_ROOT}/gitlab.com/theuberlab/gohget`

## Code Root
Goh! Get! however is much more forgiving with the URL. `go get` will only
accept an argument formatted the same as an include statement in a go program.
Goh! Get! on the other hand allows for a leading schema and attempts to
correct for trailing path elements such as the URL to a file in a repository
instead of the repository itself.

Goh! Get!, for example, will successfully clone

Goh! Get! will choose a destination directory using the following logic.

1. Set the base directory
   1. If set use the environment variable `GOH_CODE_ROOT`
   2. If `GOH_CODE_ROOT` is not set check for a `GOPATH` variable and fall
       back to `${GOPATH}/src`
   3. If neither variable is set fall back to `${HOME}\code`
   4. If none of the above are set error.
1. Attempt to parse the requested URL and determine the intended repository
   from the presence of optional elements such as a leading schema, trailing
   `.git`, etc.
   3. Error if the URL cannot be parsed
1. If the host portion of the URL starts with 'gitlab' build a gitlab
    https clone URL manually. This is primarily to address [this issue here](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/37832)
1. If the url is an ssh clone url, 'git@<host>:<path', parse it to determine
   the destination directory then call `git clone <provided ssh url> <parsed directory path>`
1. If the supplied repository is neither an ssh nor a gitlab url determine
   the clone url as follows.
   6. Add or replace the schema with 'https://'
   7. Append '?go-get=1' to the URL.
   8. Request the compiled URL and parse out the meta tag named 'go-import'.
       The 'go-import' meta tag looks like the following.
```
<meta name="go-import" content="github.com/prometheus/alertmanager git https://github.com/prometheus/alertmanager.git">
```

## Installation
The simplest way to install Goh! Get! is to use the `go` build tool. Assuming
that you have '${GOPATH}/bin' in your `PATH` simply issue

`go install gitlab.com/theuberlab/gohget/`

Then you're ready to go(hget).

```
[sporkboy@slepnir:~:]
$ export GOH_CODE_ROOT=$HOME/go/src
[sporkboy@slepnir:~:]
$ gohget https://github.com/prometheus/prometheus
level=error ts=2021-01-01T18:17:33.258255-08:00 caller="lug internal" Message="InitLoggerFromYaml: Could not read file. Initializing default logger." Filename=lugConfig.yml Error="config file lugConfig.yml does not exist"
Cloned project https://github.com/prometheus/prometheus to:
/Users/sporkboy/go/src/github.com/prometheus/prometheus
[sporkboy@slepnir:~:]
$ 
```

Altertatively download a pre-compiled binary for your favorite platform:

Linux: [ ![Download](https://api.bintray.com/packages/theuberlab/gohget/gohget_linux_amd64/images/download.svg) ](https://bintray.com/theuberlab/gohget/gohget_linux_amd64/_latestVersion)

MacOS: [ ![Download](https://api.bintray.com/packages/theuberlab/gohget/gohget_darwin_amd64/images/download.svg) ](https://bintray.com/theuberlab/gohget/gohget_darwin_amd64/_latestVersion)

Windows: [ ![Download](https://api.bintray.com/packages/theuberlab/gohget/gohget_windows_amd64/images/download.svg) ](https://bintray.com/theuberlab/gohget/gohget_windows_amd64/_latestVersion)
