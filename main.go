package main

import (
	"fmt"
	"gopkg.in/alecthomas/kingpin.v2"

	"os"

	"gitlab.com/theuberlab/gohget/pkg/fetchers"
	"gitlab.com/theuberlab/gohget/pkg/translators"
	"gitlab.com/theuberlab/gohget/pkg/utility"
	"gitlab.com/theuberlab/lug"
)

// General variables
var (
	logLevel     string
	logFormat    string
	logUTC       bool
	appVersion   string
	repoString   string
	codeRootDir  string
	transControl *translators.TranslatorController
)

// Variables that will be used as handles for specific activities.
var (
	// Creates the top level context for all commands flags and arguments
	app = kingpin.New("gohget", "Goh! Get! is a small tool which mimics the behavior of `go get` without some of it's limitations")
)

// Initialize some components that need to be available early on.
func init() {
	appVersion = utility.Version.GetVersionString()

	// Setup Kingpin flags
	// Set the application version number
	app.Version(appVersion)
	// Allow -h as well as --help
	app.HelpFlag.Short('h')

	// Logging flags
	app.Flag("log-utc", "Timestamp lug.Log messages in UTC instead of local time. Can be set via GOH_LOG_UTC").Default("false").Envar("GOH_LOG_UTC").BoolVar(&logUTC)
	app.Flag("logLevel", "The level of lug.Logging to use. Can be set via GOH_LOG_LEVEL").Short('l').Default("Error").Envar("GOH_LOG_LEVEL").EnumVar(&logLevel, "None",
		"Error",
		"Warn",
		"Info",
		"Debug",
		"All")
	app.Flag("logformat", "What output format to use. Can be set via GOH_LOG_FMT").Short('f').Default("logfmt").Envar("GOH_LOG_FMT").EnumVar(&logFormat, "logfmt", "json")

	var defaultDir string
	goDir, exists := os.LookupEnv("GOPATH")

	if !exists || goDir == "" {
		homeDir, exist := os.LookupEnv("HOME")
		if !exist || homeDir == "" {
			fmt.Println("One of environment variables GOH_CODE_ROOT, GOPATH or HOME must be set")
			app.Usage(os.Args[1:])
		} else {
			defaultDir = homeDir + "code"
		}
	} else {
		defaultDir = goDir + "/src"
	}

	app.Flag("code-root", "The root directory to check out projects into. Defaults to $GOPATH/src (if $GOPATH is set) or $HOME/code. Should be set via GOH_CODE_ROOT").Default(defaultDir).Envar("GOH_CODE_ROOT").StringVar(&codeRootDir)

	// What we care about
	app.Arg("repository", "The git repository to fetch.").Required().StringVar(&repoString)

	// Now parse the flags
	kingpin.MustParse(app.Parse(os.Args[1:]))

	lug.InitLoggerFromYaml("lugConfig.yml")
}

func main() {
	_ = lug.Trace("Message", "Running")

	transCon := translators.NewDefaultController()

	goAddr := transCon.TranslateAddress(repoString)

	destDir := fetchers.GitFetch(goAddr, codeRootDir)

	fmt.Printf("Cloned project %s to:\n%s\n", goAddr.Supplied, destDir)
}
