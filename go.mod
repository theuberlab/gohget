module gitlab.com/theuberlab/gohget

go 1.15

require (
	github.com/go-test/deep v1.0.7
	github.com/magefile/mage v1.11.0
	github.com/prometheus/common v0.15.0
	gitlab.com/theuberlab/lug v0.9.2
	gopkg.in/alecthomas/kingpin.v2 v2.2.6
)
