package bintray

import (
	"bytes"
	"github.com/magefile/mage/mg"
	"github.com/magefile/mage/sh"
	"net/http"
	"os"
	"os/exec"
	"runtime"
	"strings"

	"github.com/prometheus/common/log"
)

var (
	repoBase        string
	buildDir        string
	buildDirName    = "build"
	binName         = "gohget"
	bintrayOrgName  = "theuberlab"
	bintrayRepoName = binName
	bintrayKey      = os.Getenv("BINTRAY_API_KEY_THEUBERLAB")
)

// InitVars initializes some global variables. Do not call this target directly.
func InitVars() {
	var err error
	repoBase, err = sh.Output("git", "rev-parse", "--show-toplevel")

	if err != nil {
		log.Fatal(err)
	}
	buildDir = repoBase + "/" + buildDirName
}

// publishToBintray actually publishes the file.
func publishToBintray(pubVersion string) {
	mg.Deps(InitVars)
	//PUT /content/:subject/:repo/:package/:version/:file_path[?publish=0/1][?override=0/1][?explode=0/1]
	//
	//
	//
	//PUT /content/theuberlab/gohget/gohget-linux

	osEs := [3]string{"linux", "darwin", "windows"}

	var uploadURL string

	for _, thisOs := range osEs {
		buildBinName := binName + "_" + thisOs + "_amd64"
		uploadURL = "https://bintray.com/api/v1/content/" + bintrayOrgName + "/" + bintrayRepoName + "/" + buildBinName + "/" + pubVersion + "/" + buildBinName + "?publish=1"

		data, err := os.Open(repoBase + "/" + buildDirName + "/" + buildBinName)
		if err != nil {
			log.Fatal(err)
		}

		defer data.Close()

		req, err := http.NewRequest("PUT", uploadURL, data)
		if err != nil {
			log.Fatal(err)
		}
		req.Header.Set("Content-Type", "application/binary")
		//req.Header.Set("X-Api-Key", bintrayKey)

		req.SetBasicAuth("thesporkboy", bintrayKey)

		client := &http.Client{}
		log.Infof("Uploading %s to %s", buildBinName, uploadURL)
		res, err := client.Do(req)
		if err != nil {
			log.Fatal(err)
		}
		defer res.Body.Close()
	}
}

// ReleaseAll publishes all versions to bintray
func ReleaseAll() {
	mg.Deps(InitVars)
	var executable string
	if runtime.GOOS == "darwin" {
		executable = buildDir + "/" + "gohget_darwin_amd64"
	} else {
		executable = buildDir + "/" + "gohget_linux_amd64"
	}

	var stdout, stderr bytes.Buffer

	// `go version` command
	cmd := &exec.Cmd{
		Path:   executable,
		Args:   []string{executable, "--version"},
		Stdout: &stdout,
		Stderr: &stderr,
	}

	err := cmd.Run()
	if err != nil {
		log.Fatalf("Command [%s] failed with %s\n", cmd.String(), string(stderr.Bytes()))
	}

	// kingpin's --version prints to stderr not stdout
	vers := string(stderr.Bytes())
	vers = strings.TrimRight(vers, "\n")

	publishToBintray(vers)
}
