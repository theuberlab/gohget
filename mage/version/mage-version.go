package version

import (
	"bytes"
	"fmt"
	"os/exec"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/prometheus/common/log"
)

// GetCurrentVersion uses git describe https://git-scm.com/docs/git-describe to fetch the 'current' version.
// describe finds the most recent tag that is reachable from a commit. If the tag points to the commit, then only the tag
// is shown. Otherwise, it suffixes the tag name with the number of additional commits on top of the tagged object and the
// abbreviated object name of the most recent commit. The result is a "human-readable" object name which can also be used
// to identify the commit to other git commands.
// I.E. if there is a tag, 'v1.0.2' which points to the current commit describe will return 'v1.0.2'
// If the most recent tag is 'v1.0.2' and it is 3 commits away and the current commit is 919c2c38d2d322dcfb5bef7b64e7f03beec3c7cd
// describe will return 'v1.0.2-3-g919c2c3'
func GetCurrentVersion() string {
	executable, err := exec.LookPath("git")
	if err != nil {
		log.Fatalf("Unable to find git executable %s\n", err)
	}

	var stdout, stderr bytes.Buffer
	// `go version` command
	cmd := &exec.Cmd{
		Path:   executable,
		Args:   []string{executable, "describe"},
		Stdout: &stdout,
		Stderr: &stderr,
	}

	err = cmd.Run()
	if err != nil {
		log.Fatalf("Command [%s] failed with %s\n", cmd.String(), string(stderr.Bytes()))
	}

	version := string(stdout.Bytes())
	version = strings.TrimRight(version, "\n")
	return string(version)
}

// GetNextVersion uses `git describe` the same way as GetCurrentVersion() then increments the MINOR version by
// 1 and return the resulting version as a string
func GetNextVersion() string {
	result := "NOT INITIALIZED"

	// Get the tag via git describe

	executable, err := exec.LookPath("git")
	if err != nil {
		log.Fatalf("Unable to find git executable %s\n", err)
	}

	var stdout, stderr bytes.Buffer

	// `go version` command
	cmd := &exec.Cmd{
		Path:   executable,
		Args:   []string{executable, "describe"},
		Stdout: &stdout,
		Stderr: &stderr,
	}

	err = cmd.Run()
	if err != nil {
		log.Fatalf("Command [%s] failed with %s\n", cmd.String(), string(stderr.Bytes()))
	}

	version := string(stdout.Bytes())
	version = strings.TrimRight(version, "\n")

	log.Debugf("Got version [%s]\n", version)
	var compRegEx = regexp.MustCompile("^v([\\d])+\\.([\\d])+\\.([\\d])+(.*)$")

	match := compRegEx.FindStringSubmatch(version)

	minor, _ := strconv.Atoi(match[3])
	newMinor := minor + 1
	result = "v" + match[1] + "." + match[2] + "." + strconv.Itoa(newMinor)

	return result
}

// tagRepo applies the version tag to the repository.
func tagRepo(newVers string) error {
	currentTime := time.Now()

	tagMessage := fmt.Sprintf("Release %s at %s", newVers, currentTime.Format(time.RFC3339))

	executable, err := exec.LookPath("git")
	if err != nil {
		log.Fatalf("Unable to find git executable %s\n", err)
	}

	var stderr bytes.Buffer

	// `go version` command
	cmd := &exec.Cmd{
		Path:   executable,
		Args:   []string{executable, "tag", "-a", "-m", fmt.Sprintf("'%s'", tagMessage), newVers},
		Stderr: &stderr,
	}

	err = cmd.Run()
	if err != nil {
		log.Fatalf("Command [%s] failed with %s\n", cmd.String(), string(stderr.Bytes()))
	}

	return nil
}

// ApplyVersion applies an annotated tag with the next version number.
// Gets the most recent version number, increments it's MINOR version then applies an annotated
// tag of the new version number.
func ApplyVersion() {
	vers := GetNextVersion()

	log.Infof("Got version string %s", vers)

	err := tagRepo(vers)
	if err != nil {
		log.Fatalf("Error tagging repository %s\n", err)
	}
}
