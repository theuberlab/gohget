image: golang:latest

stages:
  - .pre                    # built in stage which always runs before other stages. NOTE: This is not supported until 12.4
  - unit                    # Tests
  - build                   # Compile the binaries
  - tag                     # Tag the version in the repo.
  - release                 # Publish the binaries
  - .post                   # Built in stage which always runs after all other stages. NOTE: This is not supported until 12.4

# This section is only for variables that we want to track in version control and which are not sensitive.
# If a variable meets either of the above criteria it should be stored under the project's settings -> CI/CD -> Variables
variables:
  # For any tempoary built files
  ARTIFACTS_DIR:              build

# From here https://gitlab.com/gitlab-org/gitlab/-/blob/v13.4.1-ee/lib/gitlab/ci/templates/Go.gitlab-ci.yml
# I don't think I'll need this bit since I'll be building with modules and the vendor dir.

#
## The problem is that to be able to use go get, one needs to put
## the repository in the $GOPATH. So for example if your gitlab domain
## is gitlab.com, and that your repository is namespace/project, and
## the default GOPATH being /go, then you'd need to have your
## repository in /go/src/gitlab.com/namespace/project
## Thus, making a symbolic link corrects this.
#before_script:
#  - mkdir -p $GOPATH/src/$(dirname $REPO_NAME)
#  - ln -svf $CI_PROJECT_DIR $GOPATH/src/$REPO_NAME
#  - cd $GOPATH/src/$REPO_NAME


# TODO: Add these later

.set_new_version:
  stage: .pre
  script:
    - mkdir ${ARTIFACTS_DIR?}
    - git describe --match "v[0-9]*" --abbrev=0 > ${ARTIFACTS_DIR?}/version.txt
  artifacts:
    expose_as: theseartifacts
    paths:
      - ${ARTIFACTS_DIR?}
  rules:
    - if: $CI_COMMIT_BRANCH == 'releases'
      when: always
    - when: never

# Notify the committer of responsibilities.
.notify:
  stage: .pre
  script:
    - Echo "Notifying committer."
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event' && $SOMEVAR == 'Some other condition.'
      when: always

test:
  stage: unit
  before_script:
    - echo "Clearing release directory."
    - rm ./releases/*
  before_script:
    # Barf and die if a tag for the version already exists.
    - if [[ $(git describe --match "v[0-9]*" --abbrev=0) == ${VERSION?} ]]; then echo "Version tag for version ${VERSION?} already exists. Aborting"; false; fi
    # Create a symbolic link under $GOPATH, this is needed for local build
    - cd $GOPATH/src
    - mkdir -p gitlab.com/$CI_PROJECT_NAMESPACE
    - cd gitlab.com/$CI_PROJECT_NAMESPACE
    - ln -s $CI_PROJECT_DIR
    - cd $CI_PROJECT_NAME
  script:
    - go fmt $(go list ./... | grep -v /vendor/)
    - go vet $(go list ./... | grep -v /vendor/)
    - go test -race -cover $(go list ./... | grep -v /vendor/)
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
      when: on_success
    - when: never

# TODO: Handle -build in version.
compile:
  stage: build
#  variables:
#    BINARIES: ${ARTIFACTS_DIR}/binaries
#    # This will only work if I'm re-cloning via ssh.
#    GIT_SSH_COMMAND: "ssh -i ${DEPLOY_SSH_KEY} -o IdentitiesOnly=yes"
  before_script:
    - if [[ $(git describe --match "v[0-9]*" --abbrev=0) == ${VERSION?} ]]; then echo "Version tag for version ${VERSION?} already exists. Aborting"; false; fi
    - echo "Clearing release directory."
    - rm ./releases/*
    # Create a symbolic link under $GOPATH, stupid hack needed for local build
    - mkdir -p ${BINARIES?}
    - mkdir release
    - cd $GOPATH/src
    - mkdir -p gitlab.com/$CI_PROJECT_NAMESPACE
    - cd gitlab.com/$CI_PROJECT_NAMESPACE
    - ln -s $CI_PROJECT_DIR
    - cd $CI_PROJECT_NAME
  script:
    - VERSIONBASE="'gitlab.com/theuberlab/common-lang/go/version."
    - VERSIONCLOSE="'"
    - MAJOR="${VERSIONBASE?}MAJOR=$(echo ${VERSION?} | cut -f1 -d'.')${VERSIONCLOSE}"
    - MINOR="${VERSIONBASE?}MINOR=$(echo ${VERSION?} | cut -f2 -d'.')${VERSIONCLOSE}"
    - PATCH="${VERSIONBASE?}PATCH=$(echo ${VERSION?} | cut -f3 -d'.')${VERSIONCLOSE}"
    - echo "Repo URL is ${CI_REPOSITORY_URL?}"
    - echo "Gitlab features ${GITLAB_FEATURES}"
    - echo ""
    - echo "Building ${VERSION?} with ldflags $MAJOR $MINOR $PATCH"
    - echo "Building for MacOS"
    - env GOOS=darwin GOARCH=amd64 go build -ldflags "-extldflags '-static'" -ldflags="-X gitlab.com/theuberlab/common-lang/go/version.Major=${MAJOR?}" -ldflags="-X gitlab.com/theuberlab/common-lang/go/version.Minor=${MINOR?}" -ldflags="-X gitlab.com/theuberlab/common-lang/go/version.Patch=${PATCH?}" -ldflags="-X gitlab.com/theuberlab/common-lang/go/version.Build=${CI_JOB_ID?}" -o releases/gohget-darwin-amd64
    - echo "Building for Linux"
    - env GOOS=linux GOARCH=amd64 go build -ldflags "-extldflags '-static'" -ldflags="-X ${MAJOR}" -ldflags="-X ${MINOR}" -ldflags="-X ${PATCH}" -o releases/gohget-linux-amd64
    - echo "Building for Windows"
    - env GOOS=windows GOARCH=amd64 go build -ldflags "-extldflags '-static'" -ldflags="-X ${MAJOR}" -ldflags="-X ${MINOR}" -ldflags="-X ${PATCH}" -o releases/gohget-win-amd64.exe
    - echo ""
    - echo "Tagging build."
    - echo "Adding release binaries to repo (yes this is a stupid hack.)"
    - PUSH_REPO_URL=$(echo $CI_REPOSITORY_URL | sed -e "s,\(https://gitlab-ci-token:\)\(.*\)\(@.*\),https://sbjorkensen:${GITLAB_API_TOKEN?}\3,")
    - git add releases/
    - git -c user.name='Sven Bjorkensen' -c user.email='sbjorkensen@theuberlab.com' commit -m'Compiled binaries.'
    - git remote add release ${PUSH_REPO_URL?}
    - git -c user.name='Sven Bjorkensen' -c user.email='sbjorkensen@theuberlab.com' tag -a v${VERSION?} -m'Tag created by pipeline for release.'
    - git push release v${VERSION?} --push-option=ci.skip
  rules:
    - if: $CI_COMMIT_BRANCH == 'master'
      when: on_success
    - when: never

create_release:
  stage: release
  script:
    - VERSION_DASH=$(echo ${VERSION?} | tr '.' '-')
    - URL_BASE="https://gitlab.com/theuberlab/gohget/-/raw/releases/releases/"
    - |
      cat << EndOFTemplate > release.json
      {
        "name": "v${VERSION?}",
        "tag_name": "v${VERSION?}",
        "description": "This \"Release\" also doesn't actually do anything. It's more of an exercise so that I can figure out how to automate these with gitlab.",
        "assets": {
          "links": [
            {
              "name": "Linux Binary",
              "url": "https://gitlab.com/theuberlab/gohget/-/raw/v${VERSION?}/releases/gohget-linux-amd64?inline=false",
              "link_type": "other"
            },
            {
              "name": "MacOS Binary",
              "url": "https://gitlab.com/theuberlab/gohget/-/raw/v${VERSION?}/releases/gohget-darwin-amd64?inline=false",
              "link_type": "other"
            },
            {
              "name": "Windows Binary",
              "url": "https://gitlab.com/theuberlab/gohget/-/raw/v${VERSION?}/releases/gohget-win-amd64.exe?inline=false",
              "link_type": "other"
            }
          ]
        }
      }
      EndOFTemplate
    - echo "Releases yamllooks like:"
    - cat release.json
    - >
      CODE=$(curl -s -o /tmp/validationbody.out -w "%{http_code}" -H 'Content-Type: application/json' -H "Private-Token: ${GITLAB_API_TOKEN?}" -d @release.json "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID?}/releases")
    - >
      if [[ "${CODE?}" != "201" ]]; then
        echo "Error creating release; response code ${CODE?} Error body:"
        cat /tmp/validationbody.out
        false
      fi
  rules:
    - if: $CI_COMMIT_BRANCH == 'master'
      when: on_success
    - when: never

# https://gitlab.com/theuberlab/gohget/-/raw/releases/releases/gohget-darwin-amd64?inline=false

## TODO: This probably shouldn't be a separate job because then we're storing the binaries multiple times. It should be part of the compile job which obviously needs a new name.
#release:
#
#Do this thing here:
#'https://stackoverflow.com/questions/29013457/how-to-store-releases-binaries-in-gitlab'
#
#Be sure and peep the second answer which looks like it includes a bash script which does it.
#
# https://gitlab.com/theuberlab/gohget/-/raw/v0.0.7/releases/gohget-darwin-amd64-0.0.7?inline=false
# https://gitlab.com/theuberlab/gohget/-/raw/v0.0.7/releases/gohget-darwin-amd64?inline=false