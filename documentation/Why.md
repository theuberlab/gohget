# Why
When I started writing go code there were certain things about the golang workflow which very much appealed to me.

At a high level that workflow is (or was) pretty much like this:

  * All code lives in a git repository available to you on the network.
  * Documentation for a project lives at the same URL as the git repository.
    * Remember the days when you would find a project somewhere on the internet then have to figure out where it was
    hosted? Maybe they had a sourceforge account, maybe you had to download a zip file and unpack it.
    * Or the reverse, when you would find code in sourceforge or github and have no idea how to use it because the
    documentation was on some other site and there was no reference to it in the repo?
    * Those are pretty much gone.
  * All code, once downloaded, lives under a common directory (${GOPATH}/src)
    * This took some getting used to but keep reading, it's worth it.
  * Instead of cloning a bunch of git repositories in random places on your working machine and either developing your
  own organizational scheme, not remembering where you cloned something into or a myriad of other organizational issues
  all code is organized by the URL that you, the human, access the repository at.

I.E. If you were to go look at the public git project for golang you would navigate to https://github.com/golang/go.
To clone that you don't need to know that the https git url is https://github.com/golang/go.git. Or have to remember to
type `git@` on the front of your URLS. Or perhaps remember the `git@` but then forget to change
`gitlab.com/theuberlab/thewolf/thewolf` into `gitlab.com:theuberlab/thewolf/thewolf.git` (Note the ':')

You simply type `go get github.com/golang/go` or `go get gitlab.com/theuberlab/thewolf/thewolf` and the `go` tool will
check out your code to `${HOME}/go/src/github.com/golang/go` or `${HOME}/go/src/gitlab.com/theuberlab/thewolf/thewolf`
and attempt to build it (assuming your $GOPATH environment variable is set to $HOME/go.)

But the go get command has a few limitations. Most of them are simple annoyances but others can be more significant.

  1) You have to have golang installed. Not an issue for me but maybe you don't write golang code and you'd like to
  adopt the same organizational strategy.
  1) If you simply copy the URL out of your browser you likely have an `https://` on the front or possibly something like
  `/src/master/` or `/-/tree/master/` on the end. None of which makes `go get` happy.
  1) Certain flavors of git *cough* gitlab  have... features... which make go get not work with subgroups. The result
  being that if either the group or the subgroup https://gitlab.com/theuberlab/thewolf/ (theuberlab being the group
  and thewolf being the subgroup in gitlab parlance) you would not be able to `go get gitlab.com/theuberlab/thewolf/thewolf`
  or `go get gitlab.com/theuberlab/thewolf/wolfplugins` even if you were the owner of said projects (git repositories.)
  1) Last but certainly not least, since the advent of [go modules](https://github.com/golang/go/wiki/Modules) the behavior of `go get` has changed
  significantly. Speicifically if you have go modules enabled (which you should) downloaded packages (checked out repos)
  no longer go to $GOPATH/src making it less useful for organizing non-go code.


Note: Much of the above is an over generalization some of which borders on flat wrong. For example `go get` actually
supports a myriad of version control systems (mercurial, subversion and some I've never heard of in addition to git.)
But git (and github specifically) is the most popular so for the purposes of discussion it's close enough.

Minor issues with leading and trailing words aside issues with go modules and private repos in subgroups are a much more
significant impactor on my own workflow.

# Gitlab "Feature"
A big part of go's ability to determine the url to pass to source control (and which source control to use) these days
relies on the presence of a meta tag called go-import.

From `go help importpath`

```
For example,

	import "example.org/pkg/foo"

will result in the following requests:

	https://example.org/pkg/foo?go-get=1
```

[This issue here](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/37832) describes the behavior. The short version
is that when go makes the initial https request for  say `gitlab.com/theuberlab/myprivatesubgroup/myprivateproject?go-get=1`
it is making an un-authenticated request. Gitlab considers this exposure a security risk and so returns
"the first two components as if it were a simple `namespace/project` path" Which [you can see in the code here](https://gitlab.com/gitlab-org/gitlab-foss/-/blob/13-4-stable/lib/gitlab/middleware/go.rb#L105)

If you simply want to make `go get` work for these you can add `.git` to the end of the repo which causes the tooling to
realize that this is a git repository and check for credentials (which may go beyond simply ~/.git-credentials) before
making any requests.

However this also results in `example.org/pkg/foo` living at `$GOTPATH/src/example.org/pkg/foo.git` instead of
`$GOTPATH/src/example.org/pkg/foo`

So if this trailing `.git` annoys you like it does me (yes, I am a little OCD) or you simply don't want to install golang
but like the idea of organizting code in this manner this might be the tool for you.
