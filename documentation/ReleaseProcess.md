# Release Process

Started a mermaid graph.
This is the entire workflow including human stuff.

I probably should have started simpler with just a pipeline workflow.

```mermaid
graph TD
  BEGIN[fa:fa-user Decide to submit code] --> A
  A(Cfa:fa-user heck out main) -->|clone, fetch, pull, etc.| B
  B(fa:fa-user Create local branch) --> C
  C(fa:fa-user Write code) --> D
  D(fa:fa-user Run Unit Tests) --> E
  E{"Tests Pass?"} --> FN
  E --> FY
  FN[No] --> C
  FY[Yes] --> G
  G(fa:fa-user Commit and push branch) --> H
  H(fa:fa-user Submit MR) --> I
  I[/fa:fa-magic Pipeline Runs Tests/] --> J
  J{Pipeline tests pass?} --> KN
  J --> KY
  KN[No] --> C
  KY[Yes] --> L
  L{Merge Request Approved?} --> MN
  L --> MY
  L --> MR
  MN[No] --> N(fa:fa-user Wait a while) --> L
  MY[Yes] --> O(fa:fa-user Rejoice) --> WAITFORRELEASE
  MR[Rejected] --> C
  MY --> P
  P[fa:fa-magic Code merged to main] --> Q
  Q[jfa:fa-magic Compile Binary for each platform] --> R
  R[fa:fa-magic stuff] --> END

  END(Rejoice even more)
```

## Building Using TheUberlab's Common Version library

I may or may not have updated version to take a single string arg instead  
of 4 separate fields so confirm that.

`go build -ldflags="-X 'gitlab.com/theuberlab/common-go/version/VersionInfo.Major=1'"  .`
